<?php

namespace Drupal\creditreform\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Creditreform settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'creditreform_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['creditreform.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // API Endpoint.
    $form['creditreform_api_endpoint'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API Endpoint'),
      '#default_value' => $this->config('creditreform.settings')->get(
        'creditreform_api_endpoint'
      ),
      '#description'   => $this->t(
        'The API endpoint for Creditreform. eg https://webservice.crediconnect.ch/V2/03/CrediConnect.svc/ws'
      ),
    ];

    // API User.
    $form['creditreform_api_user'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API User'),
      '#default_value' => $this->config('creditreform.settings')->get(
        'creditreform_api_user'
      ),
      '#description'   => $this->t(
        'The API user for Creditreform. eg ABC_XYZ2'
      ),
    ];

    // API Password.
    $form['creditreform_api_password'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API Password'),
      '#default_value' => $this->config('creditreform.settings')->get(
        'creditreform_api_password'
      ),
      '#description'   => $this->t(
        'The API password for Creditreform. eg 123456789'
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('creditreform_api_endpoint')) {
      // Use CreditreformApi to echo "Hello World".
      $creditreform_api = \Drupal::service('creditreform.api');
      $creditreform_api->initClient(
        $form_state->getValue('creditreform_api_user'),
        $form_state->getValue('creditreform_api_password')
      );
      $result = $creditreform_api->echo("Hello World");

      if ($result != "Hello World") {
        $form_state->setErrorByName(
          'creditreform_api_endpoint',
          $this->t(
            'The API endpoint is not valid. Please check your settings.'
          )
        );
      }
      else {
        // Messenger success notice.
        \Drupal::messenger()->addStatus(
          $this->t(
            'Success: The API settings are Working'
          )
        );
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('creditreform.settings')
      ->set('creditreform_api_endpoint', $form_state->getValue('creditreform_api_endpoint'))
      ->set('creditreform_api_user', $form_state->getValue('creditreform_api_user'))
      ->set('creditreform_api_password', $form_state->getValue('creditreform_api_password'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
