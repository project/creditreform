<?php

namespace Drupal\creditreform\Services;

/**
 * Drupal Service to interact with the Creditreform Soap-Api.
 */
class CreditreformApi {

  /**
   * Constructor to init the client.
   */
  public function __construct() {
    $this->initClient();
  }

  /**
   * Private SoapClient.
   *
   * @var client
   */
  private $client;

  /**
   * Protected Method to initialize the SoapClient.
   */
  public function initClient($user = NULL, $password = NULL) {
    $this->client = new \SoapClient(dirname(dirname(__DIR__)) . '/wsdl.wsdl');
    if (is_null($user)) {
      $user = \Drupal::config('creditreform.settings')->get(
        'creditreform_api_user'
      );
    }

    if (is_null($password)) {
      $password = \Drupal::config('creditreform.settings')->get(
        'creditreform_api_password'
      );
    }

    $header = $this->soapClientWsSecurityHeader($user, $password);
    $this->client->__setSoapHeaders($header);
  }

  /**
   * This function implements a WS-Security digest authentification for PHP.
   *
   * @param string $user
   *   The username.
   * @param string $password
   *   The password.
   *
   * @return \SoapHeader
   *   A WSSecurityHeader for PHPs SoapClient.
   */
  public function soapClientWsSecurityHeader($user, $password): \SoapHeader {
    // Creating date using yyyy-mm-ddThh:mm:ssZ format.
    $tm_created = gmdate('Y-m-d\TH:i:s\Z');
    // Only necessary if using the timestamp element.
    $tm_expires = gmdate('Y-m-d\TH:i:s\Z', gmdate('U') + 180);

    // Generating and encoding a random number.
    $simple_nonce = mt_rand();
    $encoded_nonce = base64_encode($simple_nonce);

    // Compiling WSS string.
    $passdigest = base64_encode(
      sha1($simple_nonce . $tm_created . $password, TRUE)
    );

    // Initializing namespaces.
    $ns_wsse
      = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
    $ns_wsu
      = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
    $password_type
      = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';
    $encoding_type
      = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary';

    // Creating WSS identification header using SimpleXML.
    $root = new \SimpleXMLElement('<root/>');

    $security = $root->addChild('wsse:Security', NULL, $ns_wsse);

    // The timestamp element is not required by all servers.
    $timestamp = $security->addChild('wsu:Timestamp', NULL, $ns_wsu);
    $timestamp->addAttribute('wsu:Id', 'Timestamp-28');
    $timestamp->addChild('wsu:Created', $tm_created, $ns_wsu);
    $timestamp->addChild('wsu:Expires', $tm_expires, $ns_wsu);

    $usernameToken = $security->addChild('wsse:UsernameToken', NULL, $ns_wsse);
    $usernameToken->addChild('wsse:Username', $user, $ns_wsse);
    $usernameToken->addChild('wsse:Password', $passdigest, $ns_wsse)
      ->addAttribute('Type', $password_type);
    $usernameToken->addChild('wsse:Password', $password, $ns_wsse)
      ->addAttribute(
        'Type',
        $password_type
      );
    $usernameToken->addChild('wsse:Nonce', $encoded_nonce, $ns_wsse)
      ->addAttribute('EncodingType', $encoding_type);
    $usernameToken->addChild('wsu:Created', $tm_created, $ns_wsu);

    // Recovering XML value from that object.
    $root->registerXPathNamespace('wsse', $ns_wsse);
    $full = $root->xpath('/root/wsse:Security');
    $auth = $full[0]->asXML();

    return new \SoapHeader(
      $ns_wsse,
      'Security',
      new \SoapVar($auth, XSD_ANYXML),
      TRUE
    );
  }

  /**
   * SoapWrapper for Echo.
   *
   * Just returns the String it gets as a paramter to ensure the API-Connection
   * is working.
   *
   * @param string $message
   *   The message to echo.
   *
   * @return string
   *   The echoed message.
   */
  public function echo($message) {
    $result = '';
    try {
      $result = $this->client->Echo(['message' => $message]);
    }
    catch (SoapFault $e) {
      // Write last Request to Drupal Log.
      \Drupal::logger('creditreform')->error(
        'Error while calling Creditreform API: ' . $e->getMessage()
        . ' Request:' . $this->client->__getLastRequest()
          );
    }

    return $result->EchoResult;
  }

}
